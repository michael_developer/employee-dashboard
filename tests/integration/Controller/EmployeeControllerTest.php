<?php

namespace App\Tests\integration\Controller;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class EmployeeControllerTest
 * @package App\Tests\integration\Controller
 * @author Michael O.
 */
class EmployeeControllerTest extends WebTestCase
{
    private $em;

    public function setUp()
    {
        parent::setUp();
        $kernel = self::bootKernel();
        $this->em = $kernel->getContainer()->get('test.App\EntityManagerInterface');
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testNewEmployeeAction()
    {
        $employee = $this->createEmployee();
        $response = $this->createLocalGuzzleClient('POST', '/employees',  $employee);
        $this->showResponse($response);
        $this->assertEquals('/employees/mike@test.ltd', $response->getHeader('Location')[0]);
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testDeleteEmployeeAction()
    {
        $employee = $this->createEmployee();
        $this->createLocalGuzzleClient('POST', '/employees',  $employee);
        $response = $this->createLocalGuzzleClient('DELETE', '/employees/mike@test.ltd');
        $this->showResponse($response);
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testNewEmployeeWithBadData()
    {
        $employee = array('first_name' => 'joe');
        $response = $this->createLocalGuzzleClient('POST', '/employees',  $employee);
        $body = json_decode($response->getBody(), true);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('There was a validation error', $body['title']);
        $this->assertEquals('Last name should not be blank', $body['errors'][0]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testListActionEmployeeCollection()
    {
        $employeesCollection = $this->createEmployeeCollection();

        foreach ($employeesCollection as $employee)
        {
            $this->createLocalGuzzleClient('POST', '/employees',  $employee);
        }

        $response = $this->createLocalGuzzleClient('GET', '/employees');
        $this->showResponse($response);
        $this->assertEquals(200, $response->getStatusCode());
        $employees = json_decode($response->getBody(), true);
        $this->assertEquals('Tiger', $employees[1]['first_name']);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testUpdateActionWithPatchMethod()
    {
        $employee = $this->createEmployee();
        $this->createLocalGuzzleClient('POST', '/employees',  $employee);

        $dept = array('name' => 'programming', 'department_size' => '12');
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2');

        $employeeUpdated = array('first_name' => 'Tiger', 'last_name' => 'Woods', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'hourly_rate' => '22.00', 'phone_number' => '5555555555',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        $response = $this->createLocalGuzzleClient('PATCH', '/employees/mike@test.ltd',  $employeeUpdated);
        $employeeUpdated = json_decode($response->getBody(), true);
        $this->showResponse($response);
        $this->assertEquals('Tiger', $employeeUpdated['first_name']);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testUpdateActionWithPutMethod()
    {
        $employee = $this->createEmployee();
        $this->createLocalGuzzleClient('POST', '/employees',  $employee);

        $dept = array('name' => 'programming', 'department_size' => '12');
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2');

        $employeeUpdated = array('first_name' => 'Tiger', 'last_name' => 'Woods', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'hourly_rate' => '22.00', 'phone_number' => '5555555555',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        $response = $this->createLocalGuzzleClient('PUT', '/employees/mike@test.ltd',  $employeeUpdated);
        $employeeUpdated = json_decode($response->getBody(), true);
        $this->showResponse($response);
        $this->assertEquals('Woods', $employeeUpdated['last_name']);
    }

    /**
     * @param $method
     * @param $url
     * @param null $data
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function createLocalGuzzleClient($method, $url, $data = null)
    {
        $baseUrl = 'http://localhost';
        $client = new Client();

        return $client->request($method, $baseUrl.$url, [
                'json' => $data,
                'http_errors' => false,
            ]);
    }

    /**
     * @param Response $response
     */
    private function showResponse($response)
    {
        echo $response->getStatusCode() . "\n\n";
        echo $response->getBody() . "\n\n";

        foreach ($response->getHeaders() as $name => $values) {
            echo $name . ': ' . implode(', ', $values) . "\r\n";
        }
    }

    private function createEmployee()
    {
        $dept = array('name' => 'programming', 'department_size' => '12');
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2');

        return array('first_name' => 'Joe', 'last_name' => 'One', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'date_started' => date("Y-m-d"), 'hourly_rate' => '22.00', 'phone_number' => '5144444444',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');
    }

    private function createEmployeeCollection()
    {
        $dept = array('name' => 'programming', 'department_size' => '12');
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2');

        $collection[] =  array('first_name' => 'Joe', 'last_name' => 'One', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'date_started' => date("Y-m-d"), 'hourly_rate' => '22.00', 'phone_number' => '5144444444',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        $collection[] =  array('first_name' => 'Tiger', 'last_name' => 'Woods', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'date_started' => date("Y-m-d"), 'hourly_rate' => '22.00', 'phone_number' => '5144444444',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        $collection[] =  array('first_name' => 'James', 'last_name' => 'Two', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'date_started' => date("Y-m-d"), 'hourly_rate' => '22.00', 'phone_number' => '5144444444',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        return $collection;
    }

    public function tearDown()
    {
        parent::tearDown();
        $purger = new ORMPurger($this->em);
        $purger->purge();
    }
}