<?php

namespace tests\unit\App\Service\Builder;

use App\Service\Builder\EmployeeBuilder;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class EmployeeDirectorSpec
 * @package tests\unit\App\Service\Builder
 * @author Michael O.
 */
class EmployeeDirectorSpec extends ObjectBehavior
{
    public function let(EmployeeBuilder $builder)
    {
        $this->beConstructedWith($builder);
    }

    public function it_should_return_a_new_employee(EmployeeBuilder $builder)
    {
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postalCode' => 'H2T1T2');

        $employee = array('firstName' => 'Joe', 'lastName' => 'One', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'dateStarted' => new \DateTime(), 'hourlyRate' => '22.00',
            'phoneNumber' => '5555555', 'status' => 'intermediate');

        $builder->buildCountry(Argument::type('string'))->willReturn($builder);
        $builder->buildEmail(Argument::type('string'))->willReturn($builder);
        $builder->buildAddress(Argument::type('string'), Argument::type('string'),
            Argument::type('string'))->willReturn($builder);
        $builder->buildProvince(Argument::type('string'))->willReturn($builder);
        $builder->buildName(Argument::type('string'), Argument::type('string'))->willReturn($builder);
        $builder->buildTitle(Argument::type('string'))->willReturn($builder);
        $builder->buildStartedToday()->willReturn($builder);
        $builder->buildHourlyRate(Argument::type('string'))->willReturn($builder);
        $builder->buildEncodedPassword(Argument::type('string'))->willReturn($builder);
        $builder->buildPhoneNumber(Argument::type('string'))->willReturn($builder);
        $builder->getEmployee()->willReturn($builder);

        $this->makeEmployee($employee)->shouldReturnAnInstanceOf(EmployeeBuilder::class);
    }

    public function it_should_return_a_new_employee_with_optional_params(EmployeeBuilder $builder)
    {
        $dept = array('department' => array('name' => 'programming', 'departmentSize' => '12'));
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postalCode' => 'H2T1T2');

        $employee = array('firstName' => 'Joe', 'lastName' => 'One', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => array($dept),
            'dateStarted' => new \DateTime(), 'hourlyRate' => '22.00', 'phoneNumber' => '5555555',
            'emergencyPhoneNumber' => '4444444444', 'emergencyContactName' => 'any', 'status' => 'intermediate');

        $builder->buildCountry(Argument::type('string'))->willReturn($builder);
        $builder->buildEmail(Argument::type('string'))->willReturn($builder);
        $builder->buildAddress(Argument::type('string'), Argument::type('string'),
            Argument::type('string'))->willReturn($builder);
        $builder->buildProvince(Argument::type('string'))->willReturn($builder);
        $builder->buildName(Argument::type('string'), Argument::type('string'))->willReturn($builder);
        $builder->buildTitle(Argument::type('string'))->willReturn($builder);
        $builder->buildStartedToday()->willReturn($builder);
        $builder->buildHourlyRate(Argument::type('string'))->willReturn($builder);
        $builder->buildEncodedPassword(Argument::type('string'))->willReturn($builder);
        $builder->buildPhoneNumber(Argument::type('string'))->willReturn($builder);
        $builder->withDepartments(array($dept))->shouldBeCalled();
        $builder->withEmergencyContacts(Argument::type('string'), Argument::type('string'))->willReturn($builder);
        $builder->withAvatar(Argument::type('string'))->willReturn($builder);
        $builder->getEmployee()->willReturn($builder);

        $this->makeEmployee($employee)->shouldReturnAnInstanceOf(EmployeeBuilder::class);
    }
}