<?php

namespace tests\unit\App\Service\PasswordEncoder;

use PhpSpec\ObjectBehavior;

class BCryptEncoderSpec extends ObjectBehavior
{
    public function it_should_return_encoded_password()
    {
        $password = 'password';
        $this->encode($password)->shouldHaveStringCount(60);
    }

    public function getMatchers(): array
    {
        return [
            'haveStringCount' => function ($subject, $count) {
                return strlen($subject)==$count;
            },
        ];
    }
}