<?php

namespace tests\unit\App\Service\PayCalculator;

use App\Service\PayCalculator\AnnualDeduction;
use App\Service\PayCalculator\PayPeriod;
use PhpSpec\ObjectBehavior;

class AnnualTaxableIncomeCalculatorMonthlySpec extends ObjectBehavior
{
    public function it_should_calculate_annual_taxable_income()
    {
        $pay = new PayPeriod();
        $pay->setGrossAmount(2200);
        $pay->setRegisteredPensionDeduction(0);
        $pay->setAlimonyOrMaintenancePayment(0);
        $pay->setUnionAmount(0);

        $deductions = new AnnualDeduction();
        $deductions->setChildSupportPayment(0);
        $deductions->setLivingInAPrescribedZone(0);

        $this->calculate($pay, $deductions)->shouldReturn(26400);
    }
}