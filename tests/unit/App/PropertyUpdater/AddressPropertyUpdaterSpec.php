<?php

namespace tests\unit\App\PropertyUpdater;

use App\Entity\Address;
use App\Entity\Employee;
use PhpSpec\ObjectBehavior;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class AddressPropertyUpdaterSpec
 * @package tests\unit\App\PropertyUpdater
 * @author Michael O.
 */
class AddressPropertyUpdaterSpec extends ObjectBehavior
{
    private $propertyAccessor;

    public function let()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @throws \Exception
     * @throws \TypeError
     */
    public function it_should_return_updated_address_object()
    {
        $address = new Address();
        $addressArray = array('address' => array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2'));

        $this->updateProperties($addressArray, $address)->shouldReturn($address);
    }

    public function it_should_throw_exception_if_object_is_not_an_address()
    {
        $address = new Employee();
        $addressArray = array('address' => array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2'));

        $this->shouldThrow('\Exception')->during('updateProperties', array($addressArray, $address));
    }

    public function it_should_throw_a_type_error_if_key_is_not_an_address_object_property()
    {
        $address = new Address();
        $addressArray = array('address' => array('numberss' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2'));

        $this->shouldThrow('\Exception')->during('updateProperties', array($addressArray, $address));
    }
}