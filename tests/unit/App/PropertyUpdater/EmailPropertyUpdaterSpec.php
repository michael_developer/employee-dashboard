<?php

namespace tests\unit\App\PropertyUpdater;

use App\Entity\Address;
use App\Entity\Email;
use PhpSpec\ObjectBehavior;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class EmailPropertyUpdaterSpec
 * @package tests\unit\App\PropertyUpdater
 * @author Michael O.
 */
class EmailPropertyUpdaterSpec extends ObjectBehavior
{
    private $propertyAccessor;

    public function let()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @throws \Exception
     * @throws \TypeError
     */
    public function it_should_return_updated_email_object()
    {
        $email = new Email();
        $emailArray = array('email' => array('address' => 'mike@test.ltd'));

        $this->updateProperties($emailArray, $email)->shouldReturn($email);
    }

    public function it_should_throw_exception_if_object_is_not_an_email()
    {
        $email = new Address();
        $emailArray = array('address' => 'mike@test.ltd');

        $this->shouldThrow('\Exception')->during('updateProperties', array($emailArray, $email));
    }

    public function it_should_throw_a_type_error_if_key_is_not_an_email_object_property()
    {
        $email = new Email();
        $emailArray = array('addressWrong' => 'mike@test.ltd');

        $this->shouldThrow('\Exception')->during('updateProperties', array($emailArray, $email));
    }
}