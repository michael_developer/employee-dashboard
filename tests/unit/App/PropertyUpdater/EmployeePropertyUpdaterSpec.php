<?php

namespace tests\unit\App\PropertyUpdater;

use App\Entity\Address;
use App\Entity\Employee;
use PhpSpec\ObjectBehavior;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class EmployeePropertyUpdaterSpec
 * @package tests\unit\App\PropertyUpdater
 * @author Michael O.
 */
class EmployeePropertyUpdaterSpec extends ObjectBehavior
{
    private $propertyAccessor;

    public function let()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @throws \Exception
     * @throws \TypeError
     */
    public function it_should_return_updated_employee_object()
    {
        $employee = new Employee();

        $dept = array('name' => 'programming', 'department_size' => '12');
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2');

        $employeeArray = array('first_name' => 'Joe', 'last_name' => 'One', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'date_started' => date("Y-m-d"), 'hourly_rate' => '22.00', 'phone_number' => '5144444444',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        $this->updateProperties($employeeArray, $employee)->shouldReturn($employee);
    }

    public function it_should_throw_exception_if_object_is_not_an_email()
    {
        $employee = new Address();

        $dept = array('name' => 'programming', 'department_size' => '12');
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2');

        $employeeArray = array('first_name' => 'Joe', 'last_name' => 'One', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'date_started' => date("Y-m-d"), 'hourly_rate' => '22.00', 'phone_number' => '5144444444',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        $this->shouldThrow('\Exception')->during('updateProperties', array($employeeArray, $employee));
    }

    public function it_should_throw_a_type_error_if_key_is_not_an_email_object_property()
    {
        $employee = new Employee();

        $dept = array('name' => 'programming', 'department_size' => '12');
        $email = array('address' => 'mike@test.ltd');
        $address = array('number' => '54', 'street' => '54th avenue', 'province' => 'QC', 'country' => 'Canada',
            'postal_code' => 'H2T1T2');

        $employeeArray = array('first_name_error' => 'Joe', 'last_name' => 'One', 'password' => 'newemployee', 'title' => 'Mr.',
            'address' => $address, 'email' => $email, 'avatar' => 'random', 'departments' => [$dept],
            'date_started' => date("Y-m-d"), 'hourly_rate' => '22.00', 'phone_number' => '5144444444',
            'emergency_phone_number' => '4444444444', 'emergency_contact_name' => 'any', 'status' => 'intermediate');

        $this->shouldThrow('\Exception')->during('updateProperties', array($employeeArray, $employee));
    }
}