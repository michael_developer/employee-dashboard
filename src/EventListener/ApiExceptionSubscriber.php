<?php

namespace App\EventListener;

use App\ExceptionHandler\ApiException;
use App\ExceptionHandler\ApiExceptionHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    public function apiKernelException(GetResponseForExceptionEvent $event)
    {
        $e = $event->getException();

        if($e instanceof ApiExceptionHandler)
        {
            $apiProblem = $e->getApiException();
        }
        else
        {
            $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : 500;
            $apiProblem = new ApiException($statusCode);
            if($e instanceof HttpExceptionInterface)
            {
                $apiProblem->add('detail', $e->getMessage());
            }
        }

        $response = new JsonResponse(
            $apiProblem->toArray(),
            $apiProblem->getStatusCode()
        );

        return $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => 'apiKernelException'
        );
    }
}