<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Employee
 *
 * @ORM\Table(name="employee")
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 * @Serializer\ExclusionPolicy("none")
 */
class Employee
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     * @Assert\NotBlank(message="First name should not be blank")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     * @Assert\NotBlank(message="Last name should not be blank")
     */
    private $lastName;

    /**
     *
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     * @Assert\NotBlank(message="Password should not be blank")
     */
    private $password;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     * @Assert\NotBlank(message="Title should not be blank")
     */
    private $title;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Address", cascade={"persist", "remove"}))
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * @Serializer\Type("App\Entity\Address")
     * @Assert\NotBlank(message="Address should not be blank")
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Type("string")
     */
    private $avatar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStarted", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d'>")
     * @Assert\NotBlank(message="Date started should not be blank")
     */
    private $dateStarted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnded", type="datetime", nullable=true)
     * @Serializer\Type("DateTime<'Y-m-d'>")
     */
    private $dateEnded;

    /**
     * @ORM\Column(type="float")
     * @Serializer\Type("float")
     * @Assert\NotBlank(message="Hourly rate should not be blank")
     */
    private $hourlyRate;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     * @Assert\NotBlank(message="Phone Number should not be blank")
     * @Assert\Regex(
     *     pattern="/\d{3}-d{3}-\d{4}|\d{10}/",
     *     message="The phone number is not valid, please enter in the following formats: 111-111-1111 or 1111111111"
     * )
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Type("string")
     * @Assert\Regex(
     *     pattern="/\d{3}-d{3}-\d{4}|\d{10}/",
     *     message="The emergency phone number is not valid, please enter in the following formats: 111-111-1111 or 1111111111"
     * )
     */
    private $emergencyPhoneNumber;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Type("string")
     */
    private $emergencyContactName;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Type("string")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Department", mappedBy="employee", cascade={"persist", "remove"}))
     * @Assert\NotBlank(message="Please choose at least one department for the employee")
     * @Serializer\Type("ArrayCollection<App\Entity\Department>")
     */
    private $departments;

    /**
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Email", cascade={"persist", "remove"}))
     * @ORM\JoinColumn(name="email_id", referencedColumnName="id")
     * @Serializer\Type("App\Entity\Email")
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Message", mappedBy="employeeSenders")
     * @Serializer\Exclude
     */
    private $messagesSenders;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Message", mappedBy="employeeRecipients")
     * @Serializer\Exclude
     */
    private $messagesRecipients;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pay", mappedBy="employee", cascade={"persist", "remove"}))
     * @Serializer\Type("ArrayCollection<App\Entity\Pay>")
     */
    private $pays;

    public function __construct()
    {
        $this->messagesSenders = new ArrayCollection();
        $this->messagesRecipients = new ArrayCollection();
        $this->departments = new ArrayCollection();
        $this->pays = new ArrayCollection();
    }

    public function addMessageFromSender(Message $message)
    {
        $this->messagesSenders[] = $message;
    }

    public function addMessageFromRecipient(Message $message)
    {
        $this->messagesRecipients[] = $message;
    }

    public function addDepartment(Department $department)
    {
        $this->departments[] = $department;
    }

    public function addPay(Pay $pay)
    {
        $this->pays[] = $pay;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getDateStarted()
    {
        return $this->dateStarted;
    }

    /**
     * @param mixed $dateStarted
     */
    public function setDateStarted($dateStarted): void
    {
        $this->dateStarted = $dateStarted;
    }

    /**
     * @return mixed
     */
    public function getDateEnded()
    {
        return $this->dateEnded;
    }

    /**
     * @param mixed $dateEnded
     */
    public function setDateEnded($dateEnded): void
    {
        $this->dateEnded = $dateEnded;
    }

    /**
     * @return mixed
     */
    public function getHourlyRate()
    {
        return $this->hourlyRate;
    }

    /**
     * @param mixed $hourlyRate
     */
    public function setHourlyRate($hourlyRate): void
    {
        $this->hourlyRate = $hourlyRate;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getEmergencyPhoneNumber()
    {
        return $this->emergencyPhoneNumber;
    }

    /**
     * @param mixed $emergencyPhoneNumber
     */
    public function setEmergencyPhoneNumber($emergencyPhoneNumber): void
    {
        $this->emergencyPhoneNumber = $emergencyPhoneNumber;
    }

    /**
     * @return mixed
     */
    public function getEmergencyContactName()
    {
        return $this->emergencyContactName;
    }

    /**
     * @param mixed $emergencyContactName
     */
    public function setEmergencyContactName($emergencyContactName): void
    {
        $this->emergencyContactName = $emergencyContactName;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getMessagesSenders()
    {
        return $this->messagesSenders;
    }

    /**
     * @return mixed
     */
    public function getMessagesRecipients()
    {
        return $this->messagesRecipients;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getPays()
    {
        return $this->pays;
    }
}