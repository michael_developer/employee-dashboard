<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Serializer\Type("string")
     */
    private $content;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Employee", inversedBy="messagesSender")
     * @ORM\JoinTable(name="employees_senders_messages")
     * @Serializer\Exclude
     */
    private $employeeSenders;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Employee", inversedBy="messagesRecipient")
     * @ORM\JoinTable(name="employees_recipients_messages")
     * @Serializer\Exclude
     */
    private $employeeRecipients;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_sent", type="datetime")
     */
    private $dateSent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_read", type="datetime")
     */
    private $dateRead;

    /**
     * @ORM\Column(type="boolean")
     */
    private $read;

    public function __construct()
    {
        $this->employeeSenders = new ArrayCollection();
        $this->employeeRecipients = new ArrayCollection();
    }

    public function addEmployeeSender(Employee $employee)
    {
        $employee->addMessageFromSender($this);
        $this->employeeSenders[] = $employee;
    }

    public function addEmployeeRecipient(Employee $employee)
    {
        $employee->addMessageFromRecipient($this);
        $this->employeeRecipients[] = $employee;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * @param mixed $read
     */
    public function setRead($read): void
    {
        $this->read = $read;
    }

    /**
     * @return mixed
     */
    public function getEmployeeSenders()
    {
        return $this->employeeSenders;
    }

    /**
     * @return mixed
     */
    public function getEmployeeRecipients()
    {
        return $this->employeeRecipients;
    }

    /**
     * @return \DateTime
     */
    public function getDateSent(): \DateTime
    {
        return $this->dateSent;
    }

    /**
     * @param \DateTime $dateSent
     */
    public function setDateSent(\DateTime $dateSent): void
    {
        $this->dateSent = $dateSent;
    }

    /**
     * @return \DateTime
     */
    public function getDateRead(): \DateTime
    {
        return $this->dateRead;
    }

    /**
     * @param \DateTime $dateRead
     */
    public function setDateRead(\DateTime $dateRead): void
    {
        $this->dateRead = $dateRead;
    }
}