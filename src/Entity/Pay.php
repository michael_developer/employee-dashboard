<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pay
 *
 * @ORM\Table(name="pay")
 * @ORM\Entity(repositoryClass="App\Repository\PayRepository")
 * @Serializer\ExclusionPolicy("none")
 */
class Pay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStarted", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d'>")
     * @Assert\NotBlank(message="Date should not be blank")
     */
    private $dateStarted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnded", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d'>")
     * @Assert\NotBlank(message="Date should not be blank")
     */
    private $dateEnded;

    /**
     * @ORM\Column(type="float")
     * @Serializer\Type("float")
     * @Assert\NotBlank(message="Amount should not be blank")
     */
    private $amount;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     * @Assert\NotBlank(message="Country should not be blank")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="pays")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Type("App\Entity\Employee")
     */
    private $employee;

    /**
     * @return \DateTime
     */
    public function getDateStarted(): \DateTime
    {
        return $this->dateStarted;
    }

    /**
     * @param \DateTime $dateStarted
     */
    public function setDateStarted(\DateTime $dateStarted): void
    {
        $this->dateStarted = $dateStarted;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnded(): \DateTime
    {
        return $this->dateEnded;
    }

    /**
     * @param \DateTime $dateEnded
     */
    public function setDateEnded(\DateTime $dateEnded): void
    {
        $this->dateEnded = $dateEnded;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param mixed $employee
     */
    public function setEmployee($employee): void
    {
        $this->employee = $employee;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}