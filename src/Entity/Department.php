<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Department
 *
 * @ORM\Table(name="department")
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRepository")
 */
class Department
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Type("string")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Type("integer")
     */
    private $departmentSize;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Employee", inversedBy="departments")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Type("App\Entity\Employee")
     */
    private $employee;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDepartmentSize()
    {
        return $this->departmentSize;
    }

    /**
     * @param mixed $departmentSize
     */
    public function setDepartmentSize($departmentSize): void
    {
        $this->departmentSize = $departmentSize;
    }
}