<?php


namespace App\Service\Response;

use App\ExceptionHandler\ApiException;
use App\ExceptionHandler\ApiExceptionHandler;

class ExceptionResponse
{
    public function createResponse(array $errors, $type = ApiException::TYPE_VALIDATION_ERROR, $status = 400)
    {
        $apiException = new ApiException(
            $status,
            $type
        );
        $apiException->add('errors', $errors);

        throw new ApiExceptionHandler($apiException);
    }
}