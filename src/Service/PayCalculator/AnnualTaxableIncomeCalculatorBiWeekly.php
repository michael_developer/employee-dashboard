<?php

namespace App\Service\PayCalculator;

class AnnualTaxableIncomeCalculatorBiWeekly implements AnnualTaxableIncomeCalculator
{
    public function calculate(PayPeriod $payPeriod, AnnualDeduction $annualDeduction)
    {
        return 26 * ($payPeriod->getGrossAmount() - $payPeriod->getRegisteredPensionDeduction()
                - $payPeriod->getAlimonyOrMaintenancePayment() - $payPeriod->getUnionAmount())
            - $annualDeduction->getLivingInAPrescribedZone() - $annualDeduction->getChildSupportPayment();
    }
}