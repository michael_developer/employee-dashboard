<?php

namespace App\Service\PayCalculator;

class PayPeriod
{
    private $grossAmount;
    private $registeredPensionDeduction;
    private $unionAmount;
    private $alimonyOrMaintenancePayment;

    /**
     * @return mixed
     */
    public function getGrossAmount()
    {
        return $this->grossAmount;
    }

    /**
     * @param mixed $grossAmount
     */
    public function setGrossAmount($grossAmount): void
    {
        $this->grossAmount = $grossAmount;
    }

    /**
     * @return mixed
     */
    public function getRegisteredPensionDeduction()
    {
        return $this->registeredPensionDeduction;
    }

    /**
     * @param mixed $registeredPensionDeduction
     */
    public function setRegisteredPensionDeduction($registeredPensionDeduction): void
    {
        $this->registeredPensionDeduction = $registeredPensionDeduction;
    }

    /**
     * @return mixed
     */
    public function getUnionAmount()
    {
        return $this->unionAmount;
    }

    /**
     * @param mixed $unionAmount
     */
    public function setUnionAmount($unionAmount): void
    {
        $this->unionAmount = $unionAmount;
    }

    /**
     * @return mixed
     */
    public function getAlimonyOrMaintenancePayment()
    {
        return $this->alimonyOrMaintenancePayment;
    }

    /**
     * @param mixed $alimonyOrMaintenancePayment
     */
    public function setAlimonyOrMaintenancePayment($alimonyOrMaintenancePayment): void
    {
        $this->alimonyOrMaintenancePayment = $alimonyOrMaintenancePayment;
    }
}