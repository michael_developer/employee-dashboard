<?php

namespace App\Service\PayCalculator;

class AnnualTaxableIncomeCalculatorWeekly implements AnnualTaxableIncomeCalculator
{
    public function calculate(PayPeriod $payPeriod, AnnualDeduction $annualDeduction)
    {
        return 52 * ($payPeriod->getGrossAmount() - $payPeriod->getRegisteredPensionDeduction()
                - $payPeriod->getAlimonyOrMaintenancePayment() - $payPeriod->getUnionAmount())
            - $annualDeduction->getLivingInAPrescribedZone() - $annualDeduction->getChildSupportPayment();
    }
}