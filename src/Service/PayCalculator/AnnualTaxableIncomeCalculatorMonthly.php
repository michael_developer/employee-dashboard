<?php

namespace App\Service\PayCalculator;

class AnnualTaxableIncomeCalculatorMonthly implements AnnualTaxableIncomeCalculator
{
    public function calculate(PayPeriod $payPeriod, AnnualDeduction $annualDeduction)
    {
        return 12 * ($payPeriod->getGrossAmount() - $payPeriod->getRegisteredPensionDeduction()
                - $payPeriod->getAlimonyOrMaintenancePayment() - $payPeriod->getUnionAmount())
            - $annualDeduction->getLivingInAPrescribedZone() - $annualDeduction->getChildSupportPayment();
    }
}