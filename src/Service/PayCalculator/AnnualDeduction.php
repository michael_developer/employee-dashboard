<?php

namespace App\Service\PayCalculator;

class AnnualDeduction
{
    private $childSupportPayment;
    private $livingInAPrescribedZone;

    public function __construct()
    {
        $this->childSupportPayment = 0;
        $this->livingInAPrescribedZone = 0;
    }

    /**
     * @return mixed
     */
    public function getChildSupportPayment()
    {
        return $this->childSupportPayment;
    }

    /**
     * @param mixed $childSupportPayment
     */
    public function setChildSupportPayment($childSupportPayment): void
    {
        $this->childSupportPayment = $childSupportPayment;
    }

    /**
     * @return mixed
     */
    public function getLivingInAPrescribedZone()
    {
        return $this->livingInAPrescribedZone;
    }

    /**
     * @param mixed $livingInAPrescribedZone
     */
    public function setLivingInAPrescribedZone($livingInAPrescribedZone): void
    {
        $this->livingInAPrescribedZone = $livingInAPrescribedZone;
    }
}