<?php

namespace App\Service\PayCalculator;

interface AnnualTaxableIncomeCalculator
{
    public function calculate(PayPeriod $payPeriod, AnnualDeduction $annualDeduction);
}