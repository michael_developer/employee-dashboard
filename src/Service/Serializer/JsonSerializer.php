<?php

namespace App\Service\Serializer;

use JMS\Serializer\SerializerBuilder;

class JsonSerializer implements SerializerInterface
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function serialize($data)
    {
        return $this->serializer->serialize($data, 'json');
    }

    public function deserialize($data, $class)
    {
        return $this->serializer->deserialize($data, $class, 'json');
    }
}