<?php

namespace App\Service\Serializer;

interface SerializerInterface
{
    public function serialize($data);
    public function deSerialize($data, $class);
}