<?php

namespace App\Service\PasswordEncoder;

/**
 * Class BCryptEncoder
 * @package App\Service\PasswordEncoder
 * @author Michael O.
 */
class BCryptEncoder implements PasswordEncoderInterface
{
    public function encode($passwordToEncode)
    {
        return password_hash($passwordToEncode, PASSWORD_BCRYPT);
    }
}