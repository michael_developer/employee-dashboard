<?php

namespace App\Service\PasswordEncoder;

/**
 * Interface PasswordEncoderInterface
 * @package App\Service\PasswordEncoder
 * @author Michael O.
 */
interface PasswordEncoderInterface
{
    public function encode($passwordToEncode);
}