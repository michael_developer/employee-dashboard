<?php

namespace App\Service\Builder;

use App\Entity\Address;
use App\Entity\Department;
use App\Entity\Email;
use App\Entity\Employee;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class EmployeeBuilder
 * @package App\Service\Builder
 * @author Michael O.
 */
class EmployeeBuilder implements EmployeeBuilderInterface
{
    private $email;
    private $address;
    private $employee;
    private $departments;
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->email = new Email();
        $this->address = new Address();
        $this->employee = new Employee();
        $this->departments = array();
        $this->passwordEncoder = $passwordEncoder;
    }

    public function buildEmail($address): EmployeeBuilderInterface
    {
        $this->email->setAddress($address);
        $this->employee->setEmail($this->email);
        return $this;
    }

    public function buildCountry($country): EmployeeBuilderInterface
    {
        $this->address->setCountry($country);
        return $this;
    }

    public function buildProvince($province): EmployeeBuilderInterface
    {
        $this->address->setProvince($province);
        return $this;
    }

    public function buildAddress($number, $street, $postalCode): EmployeeBuilderInterface
    {
        $this->address->setNumber($number);
        $this->address->setStreet($street);
        $this->address->setPostalCode($postalCode);
        $this->employee->setAddress($this->address);
        return $this;
    }

    public function withDepartments(array $departments)
    {
        foreach ($departments as $department)
        {
            $departmentObj = new Department();
            $departmentObj->setName($department['name']);
            $departmentObj->setDepartmentSize($department['departmentSize']);
            $this->employee->addDepartment($departmentObj);
        }
        return $this;
    }

    public function buildEncodedPassword($password): EmployeeBuilderInterface
    {
        $this->employee->setPassword($this->passwordEncoder->encodePassword($this->employee, $password));
        return $this;
    }

    public function buildName($firstName, $lastName): EmployeeBuilderInterface
    {
        $this->employee->setFirstName($firstName);
        $this->employee->setLastName($lastName);
        return $this;
    }

    public function buildPhoneNumber($phoneNumber): EmployeeBuilderInterface
    {
        $this->employee->setPhoneNumber($phoneNumber);
        return $this;
    }

    public function buildTitle($title): EmployeeBuilderInterface
    {
        $this->employee->setTitle($title);
        return $this;
    }

    public function buildStartedToday(): EmployeeBuilderInterface
    {
        $this->employee->setDateStarted(new \DateTime());
        return $this;
    }

    public function withAvatar($avatar)
    {
        $this->employee->setAvatar($avatar);
        return $this;
    }

    public function buildHourlyRate($rate): EmployeeBuilderInterface
    {
        $this->employee->setHourlyRate($rate);
        return $this;
    }

    public function withEmergencyContacts($emergencyPhoneNumber, $emergencyContactName)
    {
        $this->employee->setPhoneNumber($emergencyPhoneNumber);
        $this->employee->setEmergencyContactName($emergencyContactName);
        return $this;
    }

    public function withStatus($status)
    {
        $this->employee->setStatus($status);
        return $this;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

}