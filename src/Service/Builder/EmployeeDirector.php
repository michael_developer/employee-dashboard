<?php

namespace App\Service\Builder;

use App\Entity\Employee;;

/**
 * Class EmployeeDirector
 * @package App\Service\Builder
 * @author Michael O.
 */
class EmployeeDirector
{
    /**
     * @var EmployeeBuilder
     */
    private $employeeBuilder;

    public function __construct(EmployeeBuilderInterface $employeeBuilder)
    {
        $this->employeeBuilder = $employeeBuilder;
    }

    /**
     * @param array $data
     * @return Employee
     */
    public function makeEmployee(array $data)
    {
        $country = $data['address']['country'];
        $emailAddress = $data['email']['address'];
        $addressNumber = $data['address']['number'];
        $street = $data['address']['street'];
        $postalCode = $data['address']['postalCode'];
        $password = $data['password'];
        $province = $data['address']['province'];
        $firstName = $data['firstName'];
        $lastName = $data['lastName'];
        $title = $data['title'];
        $hourlyRate = $data['hourlyRate'];
        $phoneNumber = $data['phoneNumber'];

        $this->employeeBuilder
            ->buildCountry($country)
            ->buildEmail($emailAddress)
            ->buildAddress($addressNumber, $street, $postalCode)
            ->buildProvince($province)
            ->buildName($firstName, $lastName)
            ->buildTitle($title)
            ->buildStartedToday()
            ->buildHourlyRate($hourlyRate)
            ->buildEncodedPassword($password)
            ->buildPhoneNumber($phoneNumber);

        if(isset($data['departments']))
        {
            $departments = $data['departments'];
            $this->employeeBuilder->withDepartments($departments);
        }

        if(isset($data['emergencyPhoneNumber']) && isset($data['emergencyContactName']))
        {
            $emergencyPhoneNumber = $data['emergencyPhoneNumber'];
            $emergencyContactName = $data['emergencyContactName'];
            $this->employeeBuilder->withEmergencyContacts($emergencyPhoneNumber, $emergencyContactName);
        }

        if(isset($data['avatar']))
        {
            $avatar = $data['avatar'];
            $this->employeeBuilder->withAvatar($avatar);
        }

        return $this->employeeBuilder->getEmployee();
    }

    public function getEmployee()
    {
        return $this->employeeBuilder->getEmployee();
    }
}