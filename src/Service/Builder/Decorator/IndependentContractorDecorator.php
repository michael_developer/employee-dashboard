<?php


namespace App\Service\Builder\Decorator;


use App\Service\Builder\EmployeeBuilderInterface;

class IndependentContractorDecorator
{
    private $builder;

    public function __construct(EmployeeBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    public function setToContractor()
    {

    }
}