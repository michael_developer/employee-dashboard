<?php


namespace App\Service\Builder\Decorator;

use App\Service\Builder\EmployeeBuilderInterface;

class CanadianDecorator
{
    private $builder;

    public function __construct(EmployeeBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    public function setCountry()
    {
        $this->builder->buildCountry('canada');
    }

    public function setCurrency()
    {

    }
}