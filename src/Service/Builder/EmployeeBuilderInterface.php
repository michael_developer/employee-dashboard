<?php

namespace App\Service\Builder;

/**
 * Interface EmployeeBuilderInterface
 * @package App\Service\Builder
 * @author Michael O.
 */
interface EmployeeBuilderInterface
{
    public function buildName($fistName, $lastName) : EmployeeBuilderInterface;
    public function buildEmail($address): EmployeeBuilderInterface;
    public function buildCountry($country): EmployeeBuilderInterface;
    public function buildProvince($province): EmployeeBuilderInterface;
    public function buildAddress($number, $street, $postalCode): EmployeeBuilderInterface;
    public function buildPhoneNumber($phoneNumber): EmployeeBuilderInterface;
    public function buildTitle($title): EmployeeBuilderInterface;
    public function getEmployee();
    public function buildEncodedPassword($password): EmployeeBuilderInterface;
    public function buildStartedToday(): EmployeeBuilderInterface;
    public function buildHourlyRate($rate): EmployeeBuilderInterface;
}