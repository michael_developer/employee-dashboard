<?php

namespace App\ExceptionHandler;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiExceptionHandler extends HttpException
{
    private $apiException;
    public function __construct(ApiException $apiException, \Exception $previous = null, array $headers = array(), $code = 0)
    {
        $this->apiException = $apiException;
        $statusCode = $apiException->getStatusCode();
        $message = $apiException->getTitle();

        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    /**
     * @return string
     */
    public function getApiException()
    {
        return $this->apiException;
    }
}