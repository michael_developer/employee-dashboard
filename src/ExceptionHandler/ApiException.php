<?php

namespace App\ExceptionHandler;

use Symfony\Component\HttpFoundation\Response;

class ApiException
{
    const TYPE_VALIDATION_ERROR = "validation_error";
    const TYPE_INVALID_BODY_FORMAT = "invalid_body_format";
    const TYPE_INTERNAL_SERVER_ERROR = "internal_server_error";

    private $statusCode;
    private $title;
    private $type;
    private $data = array();

    private static $titles = array(
        self::TYPE_VALIDATION_ERROR => 'There was a validation error',
        self::TYPE_INVALID_BODY_FORMAT => 'Malformed Json sent'
    );

    public function __construct($statusCode, $type = null)
    {
        $this->statusCode = $statusCode;

        if($type == null)
        {
            $type = 'about:blank';
            $title = isset(Response::$statusTexts[$statusCode])
                ? Response::$statusTexts[$statusCode]
                : 'Unknown status code';
        }
        else
        {
            if(!isset(self::$titles[$type]))
            {
                throw new \InvalidArgumentException('No title for type '.$type);
            }
            $title = self::$titles[$type];
        }

        $this->type = $type;
        $this->title = $title;
    }

    public function add($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function toArray()
    {
        return array_merge(
            $this->data,
            array(
                'status' => $this->statusCode,
                'type' => $this->type,
                'title' => $this->title,
            )
        );
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}