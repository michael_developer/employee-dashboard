<?php

namespace App\PropertyUpdater;

use App\Entity\Address;
use Symfony\Component\PropertyAccess\PropertyAccess;

class AddressPropertyUpdater
{
    private $propertyAccessor;

    /**
     * EmployeePropertyUpdater constructor.
     */
    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * This method uses PropertyAccessor to compare an array of properties with an existing objects and sets new values
     * on the properties that have changed.
     *
     * @param array $props
     * @param $addressObj
     * @return mixed
     * @throws \Exception
     * @throws \TypeError
     */
    public function updateProperties(array $props, $addressObj)
    {
        if(!$addressObj instanceof Address)
        {
            throw new \Exception('Wrong object given : expected App\Entity\Address got '.get_class($addressObj));
        }

        foreach ($props['address'] as $addressKey => $addressProp)
        {
            if($addressProp!==null)
            {
                $this->propertyAccessor->setValue($addressObj, $addressKey, $addressProp);
            }
        }

        return $addressObj;
    }
}