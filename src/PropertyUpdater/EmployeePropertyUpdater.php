<?php

namespace App\PropertyUpdater;

use App\Entity\Employee;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class EmployeePropertyUpdater
 * @package App\PropertyUpdater
 */
class EmployeePropertyUpdater
{
    private $propertyAccessor;

    /**
     * EmployeePropertyUpdater constructor.
     */
    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * This method uses PropertyAccessor to compare an array of properties with an existing objects and sets new values
     * on the properties that have changed.
     *
     * @param array $props
     * @param $employeeObj
     * @return mixed
     * @throws \TypeError
     * @throws \Exception
     */
    public function updateProperties(array $props, $employeeObj)
    {
        if(!$employeeObj instanceof Employee)
        {
            throw new \Exception('Wrong object given : expected App\Entity\Employee got '.get_class($employeeObj));
        }
        foreach ($props as $k => $prop)
        {
            if($k!=='departments' && $prop!==null && $k!=='email' && $k!=='address')
            {
                $this->propertyAccessor->setValue($employeeObj, $k, $prop);
            }
        }
        return $employeeObj;
    }
}