<?php

namespace App\PropertyUpdater;

use App\Entity\Email;
use Symfony\Component\PropertyAccess\PropertyAccess;

class EmailPropertyUpdater
{
    private $propertyAccessor;

    /**
     * EmployeePropertyUpdater constructor.
     */
    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * This method uses PropertyAccessor to compare an array of properties with an existing objects and sets new values
     * on the properties that have changed.
     *
     * @param array $props
     * @param $emailObj
     * @return mixed
     * @throws \TypeError
     * @throws \Exception
     */
    public function updateProperties(array $props, $emailObj)
    {
        if(!$emailObj instanceof Email)
        {
            throw new \Exception('Wrong object given : expected App\Entity\Email got '.get_class($emailObj));
        }

        foreach ($props['email'] as $emailKey => $emailProp)
        {
            if($emailProp!==null)
            {
                $this->propertyAccessor->setValue($emailObj, $emailKey, $emailProp);
            }
        }
        return $emailObj;
    }
}