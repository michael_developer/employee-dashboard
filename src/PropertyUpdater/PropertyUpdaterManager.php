<?php

namespace App\PropertyUpdater;

class PropertyUpdaterManager
{
    private $addressPropertyUpdater;
    private $emailPropertyUpdater;
    private $employeePropertyUpdater;

    public function __construct(AddressPropertyUpdater $addressPropertyUpdater, EmailPropertyUpdater $emailPropertyUpdater,
                                EmployeePropertyUpdater $employeePropertyUpdater)
    {
        $this->addressPropertyUpdater = $addressPropertyUpdater;
        $this->emailPropertyUpdater = $emailPropertyUpdater;
        $this->employeePropertyUpdater = $employeePropertyUpdater;
    }

    /**
     * @return AddressPropertyUpdater
     */
    public function getAddressPropertyUpdater(): AddressPropertyUpdater
    {
        return $this->addressPropertyUpdater;
    }

    /**
     * @return EmailPropertyUpdater
     */
    public function getEmailPropertyUpdater(): EmailPropertyUpdater
    {
        return $this->emailPropertyUpdater;
    }

    /**
     * @return EmployeePropertyUpdater
     */
    public function getEmployeePropertyUpdater(): EmployeePropertyUpdater
    {
        return $this->employeePropertyUpdater;
    }
}