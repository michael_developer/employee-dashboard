<?php

namespace App\Controller;

use App\Entity\Employee;
use App\ExceptionHandler\ApiException;
use App\PropertyUpdater\PropertyUpdaterManager;
use App\Service\PasswordEncoder\PasswordEncoderInterface;
use App\Service\Response\ExceptionResponse;
use App\Service\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class EmployeeController
 * @package App\Controller
 * @author Michael O.
 */
class EmployeeController extends Controller
{
    private $em;
    private $encoder;
    private $jsonSerializer;

    public function __construct(EntityManagerInterface $entityManager, PasswordEncoderInterface $encoder,
                                SerializerInterface $serializer)
    {
        $this->em = $entityManager;
        $this->encoder = $encoder;
        $this->jsonSerializer = $serializer;
    }

    /**
     * @Route("/employees", name="api_new_employee")
     * @Method("POST")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ExceptionResponse $exceptionResponse
     * @return Response
     */
    public function newAction(Request $request, ValidatorInterface $validator, ExceptionResponse $exceptionResponse)
    {
        $body = $request->getContent();

        /**
         * @var Employee $employee
         */
        $employee = $this->jsonSerializer->deSerialize($body,Employee::class);

        //Encode password in BCrypt
        $employee->setPassword($this->encoder->encode($employee->getPassword()));

        if(!$employee||empty($body))
        {
            $errors = array('Json is either malformed, empty or employee object is not valid');
            $exceptionResponse->createResponse($errors, ApiException::TYPE_INVALID_BODY_FORMAT);
        }

        $errors = $validator->validate($employee);

        if (count($errors) > 0) {

            $errorsArray = array();

            foreach ($errors as $error)
            {
                $errorsArray[] = $error->getMessage();
            }

            $exceptionResponse->createResponse($errorsArray, ApiException::TYPE_VALIDATION_ERROR);
        }

        try {
            $this->em->persist($employee);
            $this->em->flush();
        } catch (\Exception $exception) {
            $exceptionResponse->createResponse(array($exception->getMessage()),
                ApiException::TYPE_VALIDATION_ERROR, 500);
        }

        $url = $this->generateUrl(
            'api_show_employee',
            array('email' => $employee->getEmail()->getAddress())
        );

        $employeeJson = $this->jsonSerializer->serialize($employee);
        $response = new Response($employeeJson, 201, array(
            'Content-Type' => 'application/json'
        ));

        $response->headers->set('Location', $url);

        return $response;
    }

    /**
     * @Route("/employees/{email}")
     * @Method({"PUT","PATCH"})
     * @param Request $request
     * @param ExceptionResponse $exceptionResponse
     * @param $email
     * @param PropertyUpdaterManager $propertyUpdaterManager
     * @return Response
     * @throws \Exception|\TypeError
     */
    public function updateAction(Request $request, PropertyUpdaterManager $propertyUpdaterManager,
                                 ExceptionResponse $exceptionResponse, $email)
    {
        $emailEmployee = $this->getDoctrine()->getRepository("App:Email")
            ->findOneBy(array('address' => $email));

        $employee = $this->getDoctrine()->getRepository("App:Employee")
            ->findOneBy(array('email' => $emailEmployee));

        if(!$emailEmployee||!$employee)
        {
            $errors = array('No employee with email '.$email);
            $exceptionResponse->createResponse($errors, ApiException::TYPE_VALIDATION_ERROR);
        }

        $body = $request->getContent();
        $props = json_decode($body, true);

        $address = $employee->getAddress();

        $propertyUpdaterManager->getEmployeePropertyUpdater()->updateProperties($props, $employee);
        $propertyUpdaterManager->getEmailPropertyUpdater()->updateProperties($props, $emailEmployee);
        $propertyUpdaterManager->getAddressPropertyUpdater()->updateProperties($props, $address);

        $this->em->flush();

        $employeeJson = $this->jsonSerializer->serialize($employee);

        return new Response($employeeJson, 200, array(
            'Content-Type' => 'application/json'
        ));
    }

    /**
     * @Route("/employees/{email}", name="api_show_employee")
     * @Method("GET")
     * @param $email
     * @return Response
     */
    public function showAction($email)
    {
        $employee = $this->getDoctrine()->getRepository("App:Employee")
            ->findOneBy(array('email' => $email));

        if (!$employee) {
            throw $this->createNotFoundException("No employee found with username ".$email);
        }
        $jsonData = $this->jsonSerializer->serialize($employee);

        $response = new Response($jsonData, 200, array(
            'Content-Type' => 'application/json'
        ));

        return $response;
    }

    /**
     * @Route("/employees")
     * @Method("GET")
     * @return Response
     */
    public function listAction()
    {
        $employees = $this->getDoctrine()->getRepository("App:Employee")
            ->findAll();

        $jsonData = $this->jsonSerializer->serialize($employees);

        $response = new Response($jsonData, 200, array(
            'Content-Type' => 'application/json'
        ));

        return $response;
    }

    /**
     * @Route("/employees/{email}")
     * @Method("DELETE")
     * @param $email
     * @return Response
     */
    public function deleteAction($email)
    {
        $email = $this->getDoctrine()->getRepository("App:Email")
            ->findOneBy(array('address' => $email));

        $employee = $this->getDoctrine()->getRepository("App:Employee")
            ->findOneBy(array('email' => $email));

        if ($employee) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($employee);
            $em->flush();
        }

        return new Response(null, 204);
    }
}