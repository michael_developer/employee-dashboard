<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180324023035 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1B0A5D4A4');
        $this->addSql('CREATE TABLE email (id INT AUTO_INCREMENT NOT NULL, address VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE company_email');
        $this->addSql('DROP INDEX UNIQ_5D9F75A1B0A5D4A4 ON employee');
        $this->addSql('ALTER TABLE employee ADD password VARCHAR(255) NOT NULL, ADD title VARCHAR(255) NOT NULL, CHANGE avatar avatar VARCHAR(255) DEFAULT NULL, CHANGE emergency_phone_number emergency_phone_number VARCHAR(255) DEFAULT NULL, CHANGE emergency_contact_name emergency_contact_name VARCHAR(255) DEFAULT NULL, CHANGE status status VARCHAR(255) DEFAULT NULL, CHANGE company_email_id email_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1A832C1C9 FOREIGN KEY (email_id) REFERENCES email (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D9F75A1A832C1C9 ON employee (email_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1A832C1C9');
        $this->addSql('CREATE TABLE company_email (id INT AUTO_INCREMENT NOT NULL, address VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE email');
        $this->addSql('DROP INDEX UNIQ_5D9F75A1A832C1C9 ON employee');
        $this->addSql('ALTER TABLE employee DROP password, DROP title, CHANGE avatar avatar VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE emergency_phone_number emergency_phone_number VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE emergency_contact_name emergency_contact_name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE status status VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE email_id company_email_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1B0A5D4A4 FOREIGN KEY (company_email_id) REFERENCES company_email (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D9F75A1B0A5D4A4 ON employee (company_email_id)');
    }
}
