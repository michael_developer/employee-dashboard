<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180324014233 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, number VARCHAR(255) NOT NULL, street VARCHAR(255) NOT NULL, postal_code VARCHAR(255) NOT NULL, province VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, address_id INT DEFAULT NULL, company_email_id INT DEFAULT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, avatar VARCHAR(255) NOT NULL, dateStarted DATETIME NOT NULL, dateEnded DATETIME NOT NULL, hourly_rate DOUBLE PRECISION NOT NULL, phone_number VARCHAR(255) NOT NULL, emergency_phone_number VARCHAR(255) NOT NULL, emergency_contact_name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_5D9F75A1F5B7AF75 (address_id), UNIQUE INDEX UNIQ_5D9F75A1B0A5D4A4 (company_email_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE department (id INT AUTO_INCREMENT NOT NULL, employee_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, department_size INT NOT NULL, INDEX IDX_CD1DE18A8C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_email (id INT AUTO_INCREMENT NOT NULL, address VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, date_sent DATETIME NOT NULL, date_read DATETIME NOT NULL, `read` TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employees_senders_messages (message_id INT NOT NULL, employee_id INT NOT NULL, INDEX IDX_14B3B6F1537A1329 (message_id), INDEX IDX_14B3B6F18C03F15C (employee_id), PRIMARY KEY(message_id, employee_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employees_recipients_messages (message_id INT NOT NULL, employee_id INT NOT NULL, INDEX IDX_B5FECDF5537A1329 (message_id), INDEX IDX_B5FECDF58C03F15C (employee_id), PRIMARY KEY(message_id, employee_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1B0A5D4A4 FOREIGN KEY (company_email_id) REFERENCES company_email (id)');
        $this->addSql('ALTER TABLE department ADD CONSTRAINT FK_CD1DE18A8C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE employees_senders_messages ADD CONSTRAINT FK_14B3B6F1537A1329 FOREIGN KEY (message_id) REFERENCES message (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employees_senders_messages ADD CONSTRAINT FK_14B3B6F18C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employees_recipients_messages ADD CONSTRAINT FK_B5FECDF5537A1329 FOREIGN KEY (message_id) REFERENCES message (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employees_recipients_messages ADD CONSTRAINT FK_B5FECDF58C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1F5B7AF75');
        $this->addSql('ALTER TABLE department DROP FOREIGN KEY FK_CD1DE18A8C03F15C');
        $this->addSql('ALTER TABLE employees_senders_messages DROP FOREIGN KEY FK_14B3B6F18C03F15C');
        $this->addSql('ALTER TABLE employees_recipients_messages DROP FOREIGN KEY FK_B5FECDF58C03F15C');
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1B0A5D4A4');
        $this->addSql('ALTER TABLE employees_senders_messages DROP FOREIGN KEY FK_14B3B6F1537A1329');
        $this->addSql('ALTER TABLE employees_recipients_messages DROP FOREIGN KEY FK_B5FECDF5537A1329');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE company_email');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE employees_senders_messages');
        $this->addSql('DROP TABLE employees_recipients_messages');
    }
}
