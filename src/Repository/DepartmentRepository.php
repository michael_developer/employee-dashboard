<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Department Repository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DepartmentRepository extends EntityRepository
{
}